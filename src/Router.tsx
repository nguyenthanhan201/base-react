import { lazy } from "react";
import { createBrowserRouter, RouteObject, RouterProvider } from "react-router-dom";
import DefaultLayout from "./shared/layouts/default-layout/DefaultLayout";

const Home = lazy(() => import("./pages/home/Home"));
const Page404 = lazy(() => import("./pages/404"));

export const routes: RouteObject[] = [
  {
    path: "/",
    element: <DefaultLayout />,
    errorElement: <Page404 />,
    children: [
      {
        index: true,
        element: <Home />,
      },
    ],
  },
];

const router = createBrowserRouter(routes);

const Router = (): React.ReactElement => {
  return <RouterProvider router={router} />;
};

export default Router;

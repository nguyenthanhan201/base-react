import { useMemo } from "react";
import { useLocation } from "react-router-dom";

export default function useStanding() {
  const location = useLocation();
  const pathnames = location.pathname.split("/");
  const isStandingCreate = useMemo(() => {
    return pathnames.includes("create");
  }, []);

  const isStandingEdit = useMemo(() => {
    return pathnames.includes("edit");
  }, []);

  const isStandingDetail = useMemo(() => {
    return pathnames.includes("detail");
  }, []);

  return {
    isStandingCreate,
    isStandingEdit,
    isStandingDetail,
  };
}

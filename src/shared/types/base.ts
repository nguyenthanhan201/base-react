export type BaseModel = {
  id: string;
  updated_at?: string;
  created_at?: string;
  deleted_at?: string;
};

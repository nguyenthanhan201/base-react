import { memo } from "react";

import LoadingIcon from "./LoadingIcon";

function Loading() {
  return (
    <div className='flex h-full w-full items-center justify-center overflow-hidden' role='status'>
      <LoadingIcon className='mr-2 h-8 w-8' />
      <span className='sr-only'>Loading...</span>
    </div>
  );
}

export default memo(Loading);

/* eslint-disable no-console */
class ConsoleLogger {
  static logDev(...args: any[]) {
    console.log("%cMode dev :", "color: blue; font-weight: bold;", ...args);
  }

  static logAPI(...args: any[]) {
    console.log("%cAPI LOG :", "color: orange; font-weight: bold;", ...args);
  }

  static logErr(...args: any[]) {
    console.error("%cMode err :", "color: blue; font-weight: bold;", ...args);
  }

  static logEvt(...args: any[]) {
    console.log("%cMode evt :", "color: blue; font-weight: bold;", ...args);
  }
}

export default ConsoleLogger;

import { Outlet } from "react-router-dom";
import DefaultFooter from "./components/DefaultFooter";
import DefaultHeader from "./components/DefaultHeader";

const DefaultLayout = () => {
  return (
    <div>
      <DefaultHeader />
      <Outlet />
      <DefaultFooter />
    </div>
  );
};

export default DefaultLayout;

export const API_URL_ENTITY = "https://staging.jcore.me/jf-entity/api/v1";
export const API_URL_ITEM = "https://staging.jcore.me/jf-item/api/v1";

export const API_URL_SKU = "https://staging.jcore.me/jf-sku/api/v1";

export const API_URL_STOCK = "https://staging.jcore.me/jf-sku/api/v1";

export const token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJXTy16Si1ndE9LN2lJR3ZqUGZrYkQ1X1hiTmNucEFqQ0RTbDI3aHhpZHlOTWxaSUYiLCJleHAiOjE2ODA2ODMzNTQsImp0aSI6ImdaVjdiQUt6TTNOeGFKTktpamwya1kyTk9nNmNuU01QTTFJaHBzSjlfa25QREdfbXBDVFBPazZoOFNkeExuZ2xCYThfcUE9PSJ9.Z9qxXZCoepPbWaFzP1-97NXIEw9x38wUO86K_shvxP8";

export const clientKey =
  "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZrd0V3WUhLb1pJemowQ0FRWUlLb1pJemowREFRY0RRZ0FFMmFBZTZ3WjBWL3hZZ0dHS2t5ZGYrUkRnSkJ0bApNSkFZQzJ2cERRSHlsQ2hiWTNhd0o2d1BUMUt0S01VdWxKWlN3U0RjMmdoQTNYV2Z1RFFnY3ZNTUxBPT0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg==";

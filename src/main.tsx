import "./styles/main.scss";

import ReactDOM from "react-dom/client";

import { Suspense } from "react";
import Router from "./Router";
// import Loading from "./shared/components/Loading/Loading";

function App() {
  return (
    // <I18nextProvider i18n={i18n}>
    <Suspense fallback={<div className='h-screen w-full'>{/* <Loading /> */}</div>}>
      <Router />
      {/* <Toast />
        <Modal /> */}
    </Suspense>
    // </I18nextProvider>
  );
}

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(<App />);

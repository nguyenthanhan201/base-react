import { Loading } from "@/shared/components/Loading";

const Home = () => {
  return (
    <>
      <h1>Home</h1>
      <Loading />
    </>
  );
};

export default Home;

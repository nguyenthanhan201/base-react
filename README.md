# base-react

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/nguyenthanhan201/base-react.git
git branch -M main
git push -uf origin main
```

## Environment

- Node - v16 (if you use nvm, you can run `nvm use` to use the correct version)
- Yarn v1
- VSCode
- Eslint plugin
- Prettier plugin

## To install dependencies

```
npm i
```

## To Start Development

```
yarn dev
```

## Code base Structure

- public: contains all static files (images, svgs, ...). That used for all project
- src/config: contains all config of project
- src/lib: contains all lib of project
- src/modules: contains all components of specially page
- src/pages: contains all pages of project
- src/routes: contains all routes of project
- src/shared: contains components/shared hooks, components, layouts, types ... . That used for all project
- src/styles: contains base styles. That used for all project
